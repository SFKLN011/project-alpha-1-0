from django.shortcuts import render, get_object_or_404, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required



# Create your views here.
@login_required
def create_task(request, project_id):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.project_id = project_id
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/list.html", context)


def task_detail(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    context = {"task": task}
    return render(request, "tasks/detail.html", context)


@login_required
def edit_application(request, id):
    task = Task.objects.get(id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
        return redirect("show_project", task.project.id)
    else:
        form = TaskForm(instance=task)

    context = {
        "task": task,
        "form": form,
    }
    return render(request, "tasks/update.html", context)

from django.db import models
from django.conf import settings


# Create your models here.
class Task(models.Model):
    company_name = models.CharField(max_length=200)
    title = models.CharField(max_length=100, null=True)
    salary = models.IntegerField(null=True)
    company_website = models.URLField(max_length=25, null=True, blank=True)
    location = models.CharField(max_length=200, null=True)
    remote_in_person_hybrid = models.CharField(max_length=100, null=True, blank=True)
    contact_info = models.CharField(max_length=200, null=True)

    project = models.ForeignKey(
        "projects.Project",
        related_name="tasks",
        on_delete=models.CASCADE,
        null=False,
    )

    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.company_name

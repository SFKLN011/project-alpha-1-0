from django.forms import ModelForm, forms
from tasks.models import Task


class TaskForm(ModelForm):

    class Meta:
        model = Task
        fields = [
            "company_name",
            "title",
            "salary",
            "company_website",
            "location",
            "remote_in_person_hybrid",
            "contact_info",
            "assignee",
        ]




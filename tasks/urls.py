from django.urls import path
from tasks.views import create_task, show_my_tasks, edit_application


urlpatterns = [
    path("create/<int:project_id>", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("edit/<int:id>/", edit_application, name="edit_application"),
]

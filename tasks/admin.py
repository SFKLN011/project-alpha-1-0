from django.contrib import admin
from tasks.models import Task


# Register your models here.
@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
            "company_name",
            "title",
            "salary",
            "company_website",
            "location",
            "remote_in_person_hybrid",
            "contact_info",
            "assignee",
    )

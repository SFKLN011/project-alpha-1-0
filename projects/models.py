from django.db import models
from django.conf import settings


# Create your models here.
class Project(models.Model):
    current_date = models.CharField(max_length=200)
    goal_applications = models.IntegerField(default=10)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.current_date

# Generated by Django 4.1.5 on 2023-01-31 02:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0009_remove_project_company_website"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="project",
            name="date_accepted",
        ),
        migrations.RemoveField(
            model_name="project",
            name="date_applied",
        ),
        migrations.RemoveField(
            model_name="project",
            name="offer_received",
        ),
    ]

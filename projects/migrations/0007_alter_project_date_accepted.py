# Generated by Django 4.1.5 on 2023-01-31 00:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0006_project_offer_received"),
    ]

    operations = [
        migrations.AlterField(
            model_name="project",
            name="date_accepted",
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from projects.forms import ProjectForm
from projects.models import Project
from tasks.models import Task
from cal.models import Event


@login_required
def project_list(request):
    projects = Project.objects.all()
    tasks = Task.objects.all()
    goal_percentage = (len(tasks) / 10) * 100
    context = {
        "projects": projects,
        "tasks": tasks,
        "goal_percentage": goal_percentage,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()

    context = {
        "project": project,
        "tasks": tasks,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


def show_calendar(request):
    return redirect("/calendar/")

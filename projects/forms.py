from django.forms import ModelForm
from projects.models import Project


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "current_date",
            "goal_applications",
            "owner",
        ]
